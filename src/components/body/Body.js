import "./body.css";
import { useState } from "react";
import sofa from "../../img/sofa.png";
import KeyBoard from "../../img/KeyBoard.png";
import WorkMedia from "../../img/WorkMedia.png";
import DDDone from "../../img/DDDone.png";
import Abstract from "../../img/Abstract.png";
import HandP from "../../img/HandP.png";
import Architect from "../../img/Architect.png";
import CalC from "../../img/CalC.png";
import Sport from "../../img/Sport.png";
import Triangle from "../../img/Triangle.png";

const Body = () => {
    const [option, setOption] = useState("Show all");
    const [count, setCount] = useState(9);
    const category = [
        {id: 1, name: "Show all"},
        {id: 2, name: "Design"},
        {id: 3, name: "Branding"},
        {id: 4, name: "Illustration"},
        {id: 5, name: "Motion"}
    ];
    const [portfolio, setPortfolio] = useState([
        {id: 1, name: "SOFA", category: "Design", src: sofa, active: false},
        {id: 2, name: "KeyBoard", category: "Branding", src: KeyBoard, active: false},
        {id: 3, name: "Work Media", category: "Illustration", src: WorkMedia, active: false},
        {id: 4, name: "DDDone", category: "Motion", src: DDDone, active: false},
        {id: 5, name: "Abstract", category: "Design", src: Abstract, active: false},
        {id: 6, name: "HandP", category: "Branding", src: HandP, active: false},
        {id: 7, name: "Architect", category: "Motion", src: Architect, active: false},
        {id: 8, name: "CalC", category: "Design", src: CalC, active: false},
        {id: 9, name: "Sport", category: "Branding", src: Sport, active: false},
        {id: 10, name: "SOFA2", category: "Design", src: sofa, active: false},
        {id: 11, name: "KeyBoard2", category: "Branding", src: KeyBoard, active: false},
        {id: 12, name: "Work Media2", category: "Illustration", src: WorkMedia, active: false},
        {id: 13, name: "DDDone2", category: "Motion", src: DDDone, active: false},
        {id: 14, name: "Abstract2", category: "Design", src: Abstract, active: false},
        {id: 15, name: "HandP2", category: "Branding", src: HandP, active: false},
        {id: 16, name: "Architect2", category: "Motion", src: Architect, active: false},
        {id: 17, name: "CalC2", category: "Design", src: CalC, active: false},
        {id: 18, name: "Sport2", category: "Branding", src: Sport, active: false}
    ]);

    const changeOption = (category) => {
        setOption(category);
    }

    const changeCount = () => {
        setCount(portfolio[portfolio.length - 1].id);
    }

    const changeActive = (item) => {
        const newPortfolio = portfolio.map((elem) => {
            if (elem.id === item.id) {
                if (elem.active === true) {
                    elem.active = false;
                } else {
                    elem.active = true;
                }

                return elem;
            }

            return elem;
        });

        setPortfolio(newPortfolio);
    }

    document.addEventListener("keydown", (e) => {
        if (e.key === "Delete") {
            const newPortfolio = portfolio.filter((item) => {
                if (item.active === false) {
                    return item;
                }
            });

            setPortfolio(newPortfolio);
        }
    })

    const showOptions = (e) => {
        if (e.currentTarget.classList.contains("portfolio__container__optionsMobile__container_active")) {
            e.currentTarget.classList.remove("portfolio__container__optionsMobile__container_active");
        } else {
            e.currentTarget.classList.add("portfolio__container__optionsMobile__container_active");
        }
    }

    return (
        <section className="portfolio">
            <div className="portfolio__container">
                <div className="portfolio__container__options">
                    {
                        category.map((item) => (
                            (item.name === option ? <button className="active" key={item.id}>{item.name}</button> : <button onClick={(e) => {changeOption(item.name)}} key={item.id}>{item.name}</button>)
                        ))
                    }
                </div>

                <div className="portfolio__container__optionsMobile">
                    <div className="portfolio__container__optionsMobile__container" onClick={(e) => {showOptions(e)}}>
                        <p>{option}</p>
                        <img src={Triangle} alt="Triangle" />
                        <div className="portfolio__container__optionsMobile__container__content">
                            {
                                category.map((item) => (
                                    (item.name !== option ? <p className="active" onClick={(e) => {changeOption(item.name)}} key={item.id}>{item.name}</p> : null)
                                ))
                            }
                        </div>
                    </div>
                </div>

                <div className="portfolio__container__content">
                    {
                        portfolio.map((item, index) => {
                            if (count >= index + 1) {
                                if (option === category[0].name) {
                                    if (item.active === false) {
                                        return <div className="portfolio__container__content__block" style={{backgroundImage: `url(${item.src})`}} key={item.id} onClick={(e) => {changeActive(item)}}>
                                            <button onClick={(e) => {changeOption(item.category)}}>{item.category}</button>
                                            <h1>{item.name}</h1>
                                        </div>
                                    } else {
                                        return <div className="portfolio__container__content__block portfolio__container__content__block_active" style={{backgroundImage: `url(${item.src})`}} key={item.id} onClick={(e) => {changeActive(item)}}>
                                            <button onClick={(e) => {changeOption(item.category)}}>{item.category}</button>
                                            <h1>{item.name}</h1>
                                        </div>
                                    }
                                } else {
                                    if (item.category === option) {
                                        if (item.active === false) {
                                            return <div className="portfolio__container__content__block" style={{backgroundImage: `url(${item.src})`}} key={item.id} onClick={(e) => {changeActive(item)}}>
                                                <button onClick={(e) => {changeOption(item.category)}}>{item.category}</button>
                                                <h1>{item.name}</h1>
                                            </div>
                                        } else {
                                            return <div className="portfolio__container__content__block portfolio__container__content__block_active" style={{backgroundImage: `url(${item.src})`}} key={item.id} onClick={(e) => {changeActive(item)}}>
                                                <button onClick={(e) => {changeOption(item.category)}}>{item.category}</button>
                                                <h1>{item.name}</h1>
                                            </div>
                                        }
                                    }
                                }
                            }
                        })
                    }
                </div>

                <div className="portfolio__container__more">
                    <button onClick={changeCount}>Load More</button>
                </div>
            </div>
        </section>
    )
}

export default Body;