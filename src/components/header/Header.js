import "./header.css";
import logo from "../../img/logo.png";
import { Link } from "react-router-dom";

const Header = () => {


    return (
        <header className="header">
            <div className="header__container">
                <div className="header__container__menu">
                    <Link to="#">
                        <img src={logo} alt="logo" />
                    </Link>

                    <ul>
                        <li>
                            <Link to="#">About</Link>
                        </li>

                        <li>
                            <Link to="#">Services</Link>
                        </li>

                        <li>
                            <Link to="#">Pricing</Link>
                        </li>

                        <li>
                            <Link to="#">Blog</Link>
                        </li>
                    </ul>

                    <button>Contact</button>
                </div>

                <div className="header__container__content">
                    <h1>Portfolio</h1>

                    <p>
                        Agency provides a full service range including technical skills, design, <br />
                        business understanding.
                    </p>
                </div>
            </div>
        </header>
    )
}

export default Header;